package Paqete;

import java.io.File;
import java.io.IOException;


public class Methods {
	public static void Creaficheros(int nFicheros, String nombre, String ncarpeta) {

		File f , d; 
		
		d = new File(ncarpeta);
		d.mkdir();
		
		for (int i = 0; i < nFicheros; i++) {
			try{
			// 1. Crear ficheros indicando la ruta
				f = new File( ncarpeta + "/" + nombre + i + ".txt");
				f.createNewFile();
				
			//2. Crear ficheros indicando la ruta con File en el constructor
			//	f = new File(d, nombre + i + ".txt");
			//	f.createNewFile();
				
			} catch (IOException e) {
				System.err.println("ERROR: " + e.getMessage());
			}
		}
	}
}
