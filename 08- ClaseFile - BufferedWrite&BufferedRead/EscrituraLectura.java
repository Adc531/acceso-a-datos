/**
 * @Project: 09 - ClaseFile - FicherosconBufferedEscrituraLectura
 * @FileName: EscrituraLectura.java
 * @Date: 18 oct 2021
 * 
 * @Author: Ra�l Delgado
 * @Email: rauldelgado2000@gmail.com
 * @GitUser: @Adc531
 * 
 * @Description: Pruebas con el Buffered Write & Buffered Read
 * 
**/

package Paquete;

import java.io.*;

public class EscrituraLectura {

	public static void EscribirDatos(String nombre, String[] tabla) {
		
		try {
			//1. Creacion del BufferedWriter
			File f = new File(nombre);
			FileWriter fw = new FileWriter(f);
			BufferedWriter BW = new BufferedWriter(fw);
			
			//2. Escritura de los datos en la tabla
			for (int i = 0; i < tabla.length; i++) {
				BW.write(tabla[i]);
				BW.newLine();
			}
			
			//3. Cerrar Fichero
			BW.close();
		} catch (IOException e) {
			System.out.println("Error: " + e.getMessage());
		}	
		catch (NullPointerException e) {
			System.out.println("Error: " + e.getMessage());
		}
	}
	
	public static void LecturaFichero(String nombre, String[] tabla) {
		
		try {
			//1. Creacion del BufferedReader
			File f = new File(nombre);
			FileReader fr = new FileReader(f);
			BufferedReader BR = new BufferedReader(fr);
			String linea;
			int posicion = 0;
			
			//2. Lectura de datos del fichero
			linea = BR.readLine();
			while(linea != null){
				tabla[posicion] = linea;
				posicion++;
				linea = BR.readLine();
			}
			
			//3. Cerrar Fichero
			BR.close();
		} catch (IOException e) {
			// TODO: handle exception
		} catch (NullPointerException e) {
			System.out.println("Error: " + e.getMessage());
		}
		
	}	
	
	public static void Escribirtabla(String[] tabla) {
		for (int i = 0; i < tabla.length; i++) {
			System.out.println(tabla[i]);
		}
	}
	
	public static void main(String[] args) {
		
		String[] Nombres = {"Pepe", "Ana", "Juan"};
		String[] TAbla = new String[3];
		String nombre = "Personas.txt";
		
		EscribirDatos(nombre, Nombres);
		LecturaFichero(nombre, TAbla);
		Escribirtabla(TAbla);

	}

}
