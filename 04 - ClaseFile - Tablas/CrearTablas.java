package Paquete;

public class CrearTablas {
	
	public static void main(String[] args) {
		
		Persona[] tabla = new Persona[5];
		String[] nombres = new String[]{"Ramon", "Pepe", "Juan", "Julia", "Paco"};
		int[] Edades = new int[]{50, 25, 80, 30, 40};
		
		for (int i = 0; i < tabla.length; i++) {
		  	
		 	tabla[i] = new Persona(nombres[i], Edades[i]);
		 	
		 }
		 
		for (int i = 0; i < tabla.length; i++) {
			
			System.out.println(i + " -> " + tabla[i].Escribir());
			
		}
		
	}
	
}
