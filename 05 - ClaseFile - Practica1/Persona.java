package Paquete;

public class Persona {
	
	private String Nombre;
	private int Edad;
	private String Direccion;
	
	public Persona(String nombre, int edad, String direccion) {
		Nombre = nombre;
		Edad = edad;
		Direccion = direccion;
	}

	public String getNombre() {
		return Nombre;
	}

	public void setNombre(String nombre) {
		Nombre = nombre;
	}

	public int getEdad() {
		return Edad;
	}

	public void setEdad(int edad) {
		Edad = edad;
	}

	public String getDireccion() {
		return Direccion;
	}

	public void setDireccion(String direccion) {
		Direccion = direccion;
	}

	public String escribir() {
		return "Persona = Nombre: " + Nombre + ", Edad: " + Edad + ", Direccion: " + Direccion;
	}
	
	
	
	
}
