package Paquete;

public class Principal {
	
	public static void main(String[] args) {
		
		Persona[] personas = new Persona[5];
		Persona aux = new Persona("", 0, "");
		Persona b = new Persona("", 0, "");
		
		Metodos.Escribir(personas);
		Metodos.Imprimir(personas);
		Metodos.Ordenar(personas, aux);
		Metodos.Mayor(personas, aux);
		//Metodos.Menor(personas, aux);
		Metodos.DireccionMayor(personas, aux);
	}
	
}
