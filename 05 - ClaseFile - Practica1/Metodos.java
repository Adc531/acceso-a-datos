package Paquete;

import java.util.Scanner;

public class Metodos {
	
		public static void Escribir(Persona[] personas) {
			Scanner sc = new Scanner(System.in);
			for (int i = 0; i < personas.length; i++) {
				sc.nextLine();
				System.out.println("Nombre: ");
				String nomb = sc.nextLine();
				System.out.println("Direccion: ");
				String dir = sc.nextLine();
				System.out.println("Edad: ");
				int edad = sc.nextInt();
				
				personas[i] = new Persona(nomb, edad, dir);
			}	
		}
		public static void Imprimir(Persona[] personas) {
			for (int i = 0; i < personas.length; i++) {
				System.out.println(personas[i].escribir());
			}
		}
		
		public static void Ordenar(Persona[] personas, Persona aux) {
			for (int i = 0; i < personas.length; i++) {
				for (int j = 0; j < personas.length - i - 1; j++) {
					if (personas[j + 1].getEdad() < personas[j].getEdad()) {
						aux = personas[j + 1];
						personas[j+1] = personas[j];
						personas[j] = aux;
					}
				}
			}
		}
		
		public static void Mayor(Persona[] personas, Persona aux) {
			
			for (int i = 0; i < personas.length; i++) {
				for (int j = 0; j < personas.length; j++) {
					if (personas[i].getEdad() > personas[j].getEdad()) {
						aux.setNombre(personas[i].getNombre());
						aux.setEdad(personas[i].getEdad());
						aux.setDireccion(personas[i].getDireccion());
					}
				}
			}
			System.out.println("La persona m�s mayor es: " + aux.escribir());
			}
		
		public static void Menor(Persona[] personas, Persona aux) {
			
			for (int i = 0; i < personas.length; i++) {
				for (int j = 0; j < personas.length; j++) {
					if (personas[i].getEdad() < personas[j].getEdad()) {
						aux.setNombre(personas[i].getNombre());
						aux.setEdad(personas[i].getEdad());
						aux.setDireccion(personas[i].getDireccion());
					}
				}
			}
			System.out.println("La persona m�s peque�a es: " + aux.escribir());
			}
		
		public static void DireccionMayor(Persona[] personas, Persona aux) {
						
			for (int i = 0; i < personas.length; i++) {
				for (int j = 0; j < personas.length; j++) {
					if (personas[i].getDireccion().length() > personas[j].getDireccion().length()) {
						aux.setNombre(personas[i].getNombre());
						aux.setEdad(personas[i].getEdad());
						aux.setDireccion(personas[i].getDireccion());
					}
				}
			}
			System.out.println("La direccion mas larga es: " + aux.escribir());
		}
}

