/**
 * @Project: 01 - EjercicioJAVA - Tablas
 * @FileName: Metodos.java
 * @Date: 13 oct 2021
 * 
 * @Author: Ra�l Delgado
 * @Email: rauldelgado2000@gmail.com
 * @GitUser: @Adc531
 * 
 * @Description: 
 * 
**/

package Paquete;

import java.util.ArrayList;
import java.util.Scanner;

public class Metodos {
public static void leerDatos(ArrayList<Persona> personas, int v) {
	Scanner values = new Scanner(System.in);
	String n, a, d;
	int e;
	
	for (int i = 0; i < v; i++) {
		
		System.out.println("Dime el nombre: ");
		n = values.nextLine();
		System.out.println("Dime el apellido: ");
		a = values.nextLine();
		System.out.println("Dime la direcci�n: ");
		d = values.nextLine();
		System.out.println("Dime la edad: ");
		e = values.nextInt();
		values.nextLine();
		
		personas.add(i, new Persona(n, a, d, e));
	}
	values.close();
}

public static void EscribirDatos(ArrayList<Persona> personas) {
	System.out.println("Aqui tienes las personas: ");
	for (int i = 0; i < personas.size(); i++) {
		System.out.println("Persona n�" + i + ": " + Persona.escribirNombre(personas.get(i)));
	}
}
public static void Mayor(ArrayList<Persona> personas) {
	Persona aux = new Persona("", "", "", 0);
	for (int i = 0; i < personas.size(); i++) {
		for (int j = 0; j < personas.size(); j++) {
			if (personas.get(i).obtenerEdad()> personas.get(j).obtenerEdad()) {
				aux.setNombre(personas.get(i).getNombre());
				aux.setApellidos(personas.get(i).getApellidos());
				aux.setDireccion(personas.get(i).obtenerDireccion());
				aux.setEdad(personas.get(i).obtenerEdad());
				
			}
		}
	}
	System.out.println("La persona m�s mayor: " + Persona.escribirNombre(aux));
}
public static void MayorDireccion(ArrayList<Persona> personas) {
	Persona aux = new Persona("", "", "", 0);
	for (int i = 0; i < personas.size(); i++) {
		for (int j = 0; j < personas.size(); j++) {
			if (personas.get(i).obtenerDireccion().length() > personas.get(j).obtenerDireccion().length()) {
				aux.setNombre(personas.get(i).getNombre());
				aux.setApellidos(personas.get(i).getApellidos());
				aux.setDireccion(personas.get(i).obtenerDireccion());
				aux.setEdad(personas.get(i).obtenerEdad());
			}
		}
	}
	System.out.println("La persona con la mayor direcci�n: " + Persona.escribirNombre(aux));
}
public static void EliminarPosicion(ArrayList<Persona> personas) {
	Scanner values = new Scanner(System.in);
	System.out.println("Elige una posicion del 0-" + personas.size() + ": ");
	int value = values.nextInt();
	personas.remove(value);
	values.close();
}
public static void EliminarPosiciones(ArrayList<Persona> personas) {
	Scanner values = new Scanner(System.in);
	System.out.println("Cuantas personas quieres borrar: ");
	int value = values.nextInt();
	for (int i = 0; i < value; i++)
		EliminarPosicion(personas);
	values.close();
}
public static void InsertarPosicion(ArrayList<Persona> personas) {
	Scanner values = new Scanner(System.in);
	int value;
	System.out.println("Elige una posicion (Tienes un rango de 0-" + personas.size() + ": )");
	value = values.nextInt();	
	personas.add(value, new Persona("", "", "", 0));
	values.close();
}
public static void InsertarPosiciones(ArrayList<Persona> personas) {
	Scanner values = new Scanner(System.in);
	int value;
	System.out.println("Que posiciones quieres borrar: ");
	value = values.nextInt(); 
	for (int i = 0; i < value; i++)
		InsertarPosicion(personas);
	
	values.close();
	
}
public static void TablaIndice(ArrayList<Persona> personas, ArrayList<Indices> indices) {
	
	for (int i = 0; i < personas.size(); i++) {
		indices.add(new Indices(personas.get(i).getNombre(), i));
	}
}

public static void OrdenaIndices(ArrayList <Indices> indices) {
	for (int i = 0; i < indices.size() - 1; i++) {
		for (int j = 0; j < indices.size() - 1 - i; j++) {
			if (indices.get(j).getNombre().compareTo(indices.get(j+1).getNombre()) > 0 ) {
				Indices aux = indices.get(j);
				indices.set(j, indices.get(j + 1));
				indices.set(j + 1, aux);
			}
		}
	}
}

public static void ImprimirTablaIndice(ArrayList<Persona> personas, ArrayList<Indices> indices) {
	OrdenaIndices(indices);
	int pos;
	for (int i = 0; i < indices.size(); i++) {
		pos = indices.get(i).getPosicion();
		System.out.println(personas.get(pos).toString());
	}
	}
}
