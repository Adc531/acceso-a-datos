/**
 * @Project: 01 - EjercicioJAVA - Tablas
 * @FileName: Menu.java
 * @Date: 13 oct 2021
 * 
 * @Author: Ra�l Delgado
 * @Email: rauldelgado2000@gmail.com
 * @GitUser: @Adc531
 * 
 * @Description: 
 * 
**/

package Paquete;
 
import java.util.ArrayList;
import java.util.Scanner;

public class Menu {
public static void menu() {
	try {
	ArrayList<Persona> personas  = new ArrayList<Persona>();
	int value, v = 10;
	ArrayList<Indices> indices = new ArrayList<Indices>();
	Scanner values= new Scanner(System.in);
	do {
		
	System.out.println("\n----------------------------------");
	System.out.println("--------    Tablas  --------------");
	System.out.println("-------  1.IniciarTabla ----------");
	System.out.println("-------  2.LeerDatos  ------------");
	System.out.println("-------  3.EscribirDatos ---------");
	System.out.println("-------  4.Mayor de edad ---------");
	System.out.println("-------  5.Mayor direccion -------");
	System.out.println("-------  6.Eliminar Posicion -----");
	System.out.println("-------  7.Eliminar Posiciones ---");
	System.out.println("-------  8.Insertar Posicion -----");
	System.out.println("-------  9.Insertar Posiciones ---");
	System.out.println("------- 10.Indice Nombre ---------");
	System.out.println("------- 11.Escribir Indices ------");
	System.out.println("----------------------------------");
	value = values.nextInt();
	values.nextLine();
	
	switch (value) {
	case 1:
		
		break;
	case 2:
		Metodos.leerDatos(personas, v);
		break;
	case 3:
		Metodos.EscribirDatos(personas);
		break;
	case 4:
		Metodos.Mayor(personas);
		break;
	case 5:
		Metodos.MayorDireccion(personas);
		break;
	case 6:
		Metodos.EliminarPosicion(personas);
		break;
	case 7:
		Metodos.EliminarPosiciones(personas);
		break;
	case 8:
		Metodos.InsertarPosicion(personas);
		break;
	case 9:
		Metodos.InsertarPosiciones(personas);
		break;
	case 10:
		Metodos.TablaIndice(personas, indices);
		break;
	case 11:
		Metodos.ImprimirTablaIndice(personas, indices);
		break;
	case 12:
		System.out.println("Has salido del programa");
		values.close();
		System.exit(0);
		break;
	}
	
	}while(value != 12 && values.hasNext());
	values.nextLine();
	values.close();
	}catch(Exception e) {System.out.println("Error: " + e.toString());}
	}
}
