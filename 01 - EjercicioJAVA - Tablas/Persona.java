/**
 * @Project: 01 - EjercicioJAVA - Tablas
 * @FileName: Persona.java
 * @Date: 13 oct 2021
 * 
 * @Author: Ra�l Delgado
 * @Email: rauldelgado2000@gmail.com
 * @GitUser: @Adc531
 * 
 * @Description: 
 * 
**/

package Paquete;

// TODO: Auto-generated Javadoc
/*
 * Persona: Incluir los siguientes m�todos
 *	o Tres constructores diferentes de la clase Persona
 *	o obtenerNombre: Devuleve el nombre y el apellido de la persona.
 *	o obtenerEdad: Devuelve la edad de la persona
 * 	o obtenerDireccion: Devuelve la direcci�n de la persona
 *	o mayorEdad: Devuelve verdadero si la persona es mayor de edad. En caso contrario, devuelve falso.
 *	o escribirNombre: Devuelve el nombre, apellidos, direcci�n y edad en ese orden.
 *	o escribirApellidos: Devuelve los apellidos, nombre, direcci�n y edad en ese orden. 
 */

/**
 * The Class Persona.
 */
public class Persona {
	
	/** The direccion. */
	private String Nombre, Apellidos, direccion;
	
	/** The edad. */
	private int edad;
	
	/**
	 * Instantiates a new persona.
	 *
	 * @param nombre the nombre
	 * @param apellidos the apellidos
	 * @param direccion the direccion
	 * @param edad the edad
	 */
	public Persona(String nombre, String apellidos, String direccion, int edad) {
		Nombre = nombre;
		Apellidos = apellidos;
		this.direccion = direccion;
		this.edad = edad;
	}

	/**
	 * Instantiates a new persona.
	 *
	 * @param nombre the nombre
	 * @param apellidos the apellidos
	 */
	public Persona(String nombre, String apellidos) {
		Nombre = nombre;
		Apellidos = apellidos;
	}

	/**
	 * Instantiates a new persona.
	 *
	 * @param edad the edad
	 */
	public Persona(int edad) {
		this.edad = edad;
	}

	/**
	 * Obtener nombre.
	 *
	 * @return the string
	 */
	public String obtenerNombre() {
		return Nombre + Apellidos;
	}

	/**
	 * Sets the nombre.
	 *
	 * @param nombre the new nombre
	 */
	public void setNombre(String nombre) {
		Nombre = nombre;
		
	}
	
	/**
	 * Gets the nombre.
	 *
	 * @return the nombre
	 */
	public String getNombre() {
		return Nombre;
	}
	
	/**
	 * Obtener apellidos.
	 *
	 * @return the string
	 */
	public String obtenerApellidos() {
		return Apellidos + " " +Nombre;
	}
	
	/**
	 * Sets the apellidos.
	 *
	 * @param apellidos the new apellidos
	 */
	public void setApellidos(String apellidos) {
		Apellidos = apellidos;
	}
	
	/**
	 * Gets the apellidos.
	 *
	 * @return the apellidos
	 */
	public String getApellidos() {
		return Apellidos;
	}

	/**
	 * Obtener direccion.
	 *
	 * @return the string
	 */
	public String obtenerDireccion() {
		return direccion;
	}

	/**
	 * Sets the direccion.
	 *
	 * @param direccion the new direccion
	 */
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	/**
	 * Obtener edad.
	 *
	 * @return the int
	 */
	public int obtenerEdad() {
		return edad;
	}

	/**
	 * Sets the edad.
	 *
	 * @param edad the new edad
	 */
	public void setEdad(int edad) {
		this.edad = edad;
	}
	
	/**
	 * Mayor edad.
	 *
	 * @return true, if successful
	 */
	public boolean MayorEdad() {
		if(edad >= 18)
			return true;
		else
			return false;
	}
	
	/**
	 * Escribir nombre.
	 *
	 * @param persona the persona
	 * @return the string
	 */
	public static String escribirNombre(Persona persona) {
		return "Nombre y Apellidos: " + persona.obtenerNombre() + ", Direccion: " + persona.obtenerDireccion() + ", Edad: " +persona.obtenerEdad();
	}
	
	/**
	 * Escribir apellidos.
	 *
	 * @param persona the persona
	 * @return the string
	 */
	public static String escribirApellidos(Persona persona) {
		return "Apellidos y Nombre: " + persona.obtenerApellidos() + ", Direccion: " + persona.obtenerDireccion() + ", Edad: " + persona.obtenerEdad();
	}
}
	

