package Ejercicio2;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

public class Principal {

public static void EscribirDatos(Persona[] personas, String nombreF) {
		
		try {
			//1. Creacion del fichero
			File f = new File(nombreF);
			FileWriter fw = new FileWriter(f);
			
			
			for (int i = 0; i < personas.length; i++) {
					
				fw.write(personas[i].toString());	
				fw.write("\n");
			}
			
			fw.close();
		} catch (Exception e) {
			// TODO: handle exception
		}	
		
	}
	
	public static void LeerDatos(Persona[] datos, String nombre) {
		try {
			File f = new File(nombre);
			FileReader fr = new FileReader(f);
			int caracter; 
			int pos = 0;
			char car;
			String value = "";
			char Separador = ';';
			caracter = fr.read();
			while (caracter != -1) {
				
				car = (char) caracter;
				
				if(car != '\n') {
					if(car != Separador)
					value += car;
					else{
						if(datos[pos].getNombre() == "") {
						datos[pos].setNombre(value);
						value = "";
						}else if(datos[pos].getDireccion() == "") {
							datos[pos].setDireccion(value);
							value = "";
						}else if (datos[pos].getEdad() == 0) {
							datos[pos].setEdad(Integer.parseInt(value));
							pos++;
							value = "";
						}
					}
				}
				
				caracter = fr.read();
			}
			fr.close();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	public static void main(String[] args) {
		Persona[] personas = new Persona[3];
			personas[0] = new Persona("Pepe", "Madrid", 19);
			personas[1] = new Persona("Ana", "Barcelona", 30);
			personas[2] = new Persona("Luis", "Vigo", 10);
		String nombreF = "Ejercicio2.txt";
		Persona[] persona = new Persona[3];
		persona[0] = new Persona("", "", 0);
		persona[1] = new Persona("", "", 0);
		persona[2] = new Persona("", "", 0);
	
		
		EscribirDatos(personas, nombreF);
		LeerDatos(persona, nombreF);
		for (int i = 0; i < 3; i++) {
			System.out.println(persona[i]);
		}
	}
	
}
