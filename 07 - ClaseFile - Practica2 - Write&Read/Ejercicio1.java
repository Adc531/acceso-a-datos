package Ejercicio1;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

public class Principal {

public static void Escribir(String Nombre, String[] datos) {
		

		try {
			//1. Creacion del fichero
			File f = new File(Nombre);
			FileWriter fw = new FileWriter(f);
			
			
			for (int i = 0; i < datos.length; i++) {
				for (int j = 0; j < datos[i].length(); j++) {
					fw.write(datos[i].charAt(j));
					
				}
				fw.write(";");
			}
			
			fw.close();
		
		
		}catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	public static void LeerDatos(String[] datos, String nombre) {
		try {
			File f = new File(nombre);
			FileReader fr = new FileReader(f);
			int caracter; 
			int pos = 0;
			char car;
			String value = "";
			char Separador = ';';
			caracter = fr.read();
			while (caracter != -1) {
				
				car = (char) caracter;
				
				if(car != Separador)
					value += car;
				else{
					datos[pos] = value;
					pos++;
					value = "";
				}
				caracter = fr.read();
			}
			fr.close();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	
	public static void main(String[] args) {
		
		String[] Nombres = {"Pepe", "Ana", "Luis"};
		String[] MisDatosLeidos = new String[Nombres.length];
		String Fichero = "Ejercicio1.txt";
		Escribir(Fichero, Nombres);
		LeerDatos(MisDatosLeidos, Fichero);
		for (int i = 0; i < MisDatosLeidos.length; i++)
			System.out.println("Posicion: "+ i + " --> " + MisDatosLeidos[i]);
		
		
	}
	
}
